import { IsNotEmpty, Length, Min, IsPositive } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
